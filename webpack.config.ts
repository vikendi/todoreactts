import * as CleanWebpackPlugin from 'clean-webpack-plugin';
import * as CopyWebpackPlugin from 'copy-webpack-plugin';
import * as HtmlWebpackPlugin from 'html-webpack-plugin';
import * as path from 'path';

export default {
    output: {
        filename: '[name].js',
        path: path.resolve('dist'),
        publicPath: '/',
    },

    resolve: {
        extensions: ['.ts', '.tsx', '.ts', '.tsx', '.js', '.jsx'],
    },

    mode: process.env.NODE_ENV || 'development',

    entry: {
        client: './src/index.tsx',
    },

    optimization: {
        splitChunks: {
            chunks: 'all',
        },
    },

    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
            },
            {
                test: /\.scss$/,
                use: [
                    'style-loader', // creates style nodes from JS strings
                    'css-loader', // translates CSS into CommonJS
                    'sass-loader', // compiles Sass to CSS, using Node Sass by default
                ],
            },
        ],

    },

    plugins: [
        new CopyWebpackPlugin([path.resolve('./static/index.html')]),
        new CleanWebpackPlugin(['dist']),
        new HtmlWebpackPlugin({
            template: './static/index.html',
        }),
    ],

    devServer: {
        historyApiFallback: true,
        hot: true,
        port: 3000,
    },

    devtool: 'cheap-module-source-map',
};
