import {action, computed, observable, reaction} from 'mobx';
import idGenerator from '../../utils/idGenerator';
import {EMAIL_REGEX} from '../constants';
import {IError, ITodo, IUserData} from '../types/interfaces';

export class AuthStore {
    @observable isLoggedIn = false;
    @observable errors: IError = {} as IError;
    @observable user: IUserData = {} as IUserData;

    validatePassword = (user: IUserData): boolean => {
        if (user.password.length < 6 || user.username.length < 2) {
            return false;
        }
        if (!user.email.match(EMAIL_REGEX)) {
            return true;
        }
        return true;
    }

    @action
    registerUser(candidate: IUserData) {
        if (this.validatePassword(candidate)) {
            this.user = candidate;
            window.localStorage.setItem('user', JSON.stringify(candidate));
        } else {
            this.errors = {error: 'Invalid data'};
            console.log(this.errors.error);
        }
    }

    @action
    logIn(candidate: IUserData) {
        const localData = window.localStorage.getItem('user');
        if (localData) {
            this.user = JSON.parse(localData) as IUserData;
            this.isLoggedIn = true;
        } else {
            console.log('NEUDACHA');
        }
    }

}

const authStore = new AuthStore();
export default authStore;
