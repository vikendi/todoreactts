import {action, computed, observable, reaction} from 'mobx';
import idGenerator from '../../utils/idGenerator';
import {ITodo} from '../types/interfaces';

export class TodoStore {
    @observable todoList: ITodo[] = [];

    constructor() {
        reaction(
            () => this.todoList.filter((todo) => !todo.isComplete),
            (incompletedTasks) => {
                if (incompletedTasks.length > 5) {
                    alert('Too much tasks');
                }
            },
        );
    }

    @computed
    get completedTasks(): number {
        return this.todoList.filter((todo) => todo.isComplete).length;
    }

    @action
    addTodo(task: string) {
        this.todoList.push({task, isComplete: false, id: idGenerator('tId')  });
    }

    @action
    completeTodo(completeTodo: ITodo) {
        // @ts-ignore
        const item = this.todoList.find((todo) => todo.id === completeTodo.id);
        item ? (item.isComplete = !item.isComplete) : null;
    }

    deleteTodo(deleteTodo: ITodo) {
        // @ts-ignore
        this.todoList = this.todoList.filter((todo: ITodo) => {
            return todo.id !== deleteTodo.id;
        });
    }

// TODO: Handle TS ingore
}

const todoStore = new TodoStore();
export default todoStore;
