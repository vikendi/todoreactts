import * as React from 'react';
import {Component} from 'react';
import './todo.scss';

import {Route, Switch} from 'react-router-dom';
import todoStore from '../store/todo.store';
import Register from './Login/register';
import {MainList} from './MainList';
import TodoAdd from './TodoAdd/todo.add';
import {TodoList} from './TodoList/todo.list';

export class App extends Component {
    render() {
        return (
            <Switch>
                <Route path='/todo' component={MainList}/>
                <Route path='/register' component={Register}/>
                <Route path='/' render={() => (<h1>OPA! MISTAKE</h1>)}/>
            </Switch>
        );
    }

}
