import {inject, observer} from 'mobx-react';
import * as React from 'react';
import {TodoStore} from '../../store/todo.store';
import {ITodo} from '../../types/interfaces';

interface ITodoListItemProps {
    todo?: ITodo;
    todoStore?: TodoStore;
}

export const TodoListItem =
    inject('todoStore')(
        observer(
            (props: ITodoListItemProps) => {
                return (<div className='view'>
                    <input
                        onChange={() => props.todoStore!.completeTodo(props.todo!)}
                        checked={props.todo!.isComplete}
                        className='toggle' type='checkbox'/>
                    <label>{props.todo!.task}</label>
                    <button className='destroy'
                            onClick={() => props.todoStore!.deleteTodo(props.todo!)}
                    />
                </div>);
            }),
    );
