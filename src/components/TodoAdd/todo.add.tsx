import { observable } from 'mobx';
import { inject, observer } from 'mobx-react';
import * as React from 'react';
import { Component } from 'react';
import {ITodoAddProps} from '../../types/interfaces';
import '../todo.scss';

@inject('todoStore')
@observer
class TodoAdd extends Component<ITodoAddProps> {
    @observable private task: string = '';

    handleTaskChange =
        ({currentTarget: { value }}: React.SyntheticEvent<HTMLSelectElement | HTMLInputElement>) => {
        this.task = value;
    }

    handleAddTodo = () => {
        this.props.todoStore!.addTodo(this.task);
        this.task = '';
    }

    handleNewTodoKeyDown(event: React.KeyboardEvent) {
        if (event.keyCode !== 13) {
            return;
        }
        this.props.todoStore!.addTodo(this.task);
        this.task = '';
    }

    render(): React.ReactNode {
        return (
            <div className='addTodo'>
                <h1>Mobx Todo</h1>
                <input className='new-todo' value={this.task}
                       placeholder='What needs to be done?'
                       onKeyDown={ (event) => this.handleNewTodoKeyDown(event) }
                       onChange={(event) => this.handleTaskChange(event)}/>
            </div>
        );
    }

}
export default TodoAdd;
