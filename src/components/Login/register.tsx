import {observable} from 'mobx';
import {inject, observer} from 'mobx-react';
import * as React from 'react';
import {Component} from 'react';
import {IAuthProps, IUserData} from '../../types/interfaces';
import './register.scss';

@inject('authStore')
@observer
class Register extends Component<IAuthProps> {
    state = {
        email: '',
        name: '',
        pass: '',
    };

    handleChange =
        ({currentTarget: {value, id}}: React.SyntheticEvent<HTMLSelectElement | HTMLInputElement>) => {
            this.setState({
                [id]: value,
            });
        }

    handleSubmit = (event: React.SyntheticEvent<HTMLSelectElement | HTMLFormElement>) => {
        event.preventDefault();
        const user: IUserData = {
            email: this.state.email,
            password: this.state.pass,
            username: this.state.name,
        } as IUserData;
        console.log(user);
        this.props.authStore!.registerUser(user);
    }

    render() {
        return (
            <div>
                <form
                    className='LoginForm'
                    onSubmit={(event) => this.handleSubmit(event)}
                >
                    <input
                        id='name'
                        type='text'
                        placeholder='Username'
                        value={this.state.name}
                        onChange={(event) => this.handleChange(event)}

                    />
                    <input
                        id='email'
                        type='email'
                        placeholder='Email'
                        value={this.state.email}
                        onChange={(event) => this.handleChange(event)}
                    />

                    <input
                        id='pass'
                        type='password'
                        placeholder='Password'
                        value={this.state.pass}
                        onChange={(event) => this.handleChange(event)}
                    />

                    <button
                        className='submit'
                        type='submit'
                    >
                        Sign in
                    </button>
                </form>
            </div>
        );
    }

}

export default Register;
