import {inject, observer} from 'mobx-react';
import * as React from 'react';
import {ITodo, ITodoListProps} from '../../types/interfaces';
import {TodoListItem} from '../TodoListItem/todo.list.item';

export const TodoList = inject('todoStore')(
    observer(
        ({todoStore}: ITodoListProps) => {
            if (!todoStore) {
                return null;
            }
            return (
                <ul className='todo-list'>
                    {todoStore.todoList.map((todo: ITodo, idx: number) => <li
                        className = {todo.isComplete ? 'completed' : undefined} // Should it be undefined there?
                        key={`${todo.id}`}>
                        <TodoListItem todo={todo}/></li>)}
                </ul>
            );
        },
    ),
);
