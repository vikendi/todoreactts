import * as React from 'react';
import {Component} from 'react';
import './todo.scss';

import todoStore from '../store/todo.store';
import TodoAdd from './TodoAdd/todo.add';
import {TodoList} from './TodoList/todo.list';

export class MainList extends Component {
    render() {
        return (
            <section className='todoapp'>
                <TodoAdd/>
                <section className='main'>
                    <TodoList/>
                </section>
            </section>

        );
    }
}
