import {AuthStore} from '../store/auth.store';
import {TodoStore} from '../store/todo.store';

export interface ITodo {
    task: string;
    isComplete: boolean;
    id: string;
}
export interface ITodoAddProps {
    todoStore?: TodoStore;
}
export interface ITodoListProps {
    todoStore?: TodoStore;
}
export interface IUserData {
    email: string;
    password: string;
    username: string;
}

export interface IError {
    error: string;
}
export interface IAuthProps {
    authStore?: AuthStore;
}
