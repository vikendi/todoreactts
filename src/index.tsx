import {Provider} from 'mobx-react';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {BrowserRouter} from 'react-router-dom';
import {App} from './components/app';
import authStore from './store/auth.store';
import todoStore from './store/todo.store';

const stores = {
    authStore,
    todoStore,
};

ReactDOM.render(
    <Provider {...stores}>
        <BrowserRouter>
            <App/>
        </BrowserRouter>
    </Provider>,
    document.getElementById('root') as HTMLElement,
)
;
